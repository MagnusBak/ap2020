**Describe your throbber design, both conceptually and technically.**

Oops, be aware: Unfortunately, my design has apparently been locked/stuck in some kind of syntax,
where the text cannot be edited (neither removed nor added). 
So I describe this in my readme, instead of inside the p5.js itself.

My design of a throbber consists mainly of a number of ellipses, in various sizes, 
which are part of a rotation, both about themselves and some overall rotational lips. 
My vision/idea of the design was – 
as you are familiar with from the "Seven Cabal" game, which is found on each type of windows computer – 
that the design itself had to be frozen in on canvas(in setup) so that one could follow in how far the rotations had come. 
Most of all, it should look like an "infinity circle” as a character in which two dragons, trying to pursue(eat) each other's tails. 
It is a recognized form of buffer, but in an updated and more abstract version, 
but with "angular" shapes, funny motifs that tease and catch the eye. 
It should be reminded of early version of animation and classic fantasy PC games.


What is your sketch? What do you want to explore and/or express?

In addition to the many ellipses of different colors, sizes and colors, t
hey rotate in the same direction, synchronously, but on opposite axis of each other, 
which forms the foundation of the throbber. Conceptually, the eternity circle works as a 
kind of work of art where you can follow the process, but still incalculable for the eye.  
Is works a little like the children’s book – Where is Waldo? 


What are the time-related syntaxes/functions that you have used in your program, 
and why have you used them in this way? How is time being constructed in computation 
(refer to both the reading materials and your process of coding)?

I would like to create a throbber who is as entertaining as it really challenges the user. 
There is an importance in a proper and real throbber deflecting and maintaining the 
concentration of the user so that they do not get the perception of time wastage *
or faulty electronics. It is important to give the user the opportunity to forget 
the time they "lose" while waiting to load in the background complete. 
I think that's fine my throbber does, as it is an "unfinished" project 
that the user himself needs to complete with their own imagination/artistic side.


Think about a throbber that you have encounted in digital culture, e.g. 
for streaming video on YouTube or loading the latest feeds on Facebook, 
or waiting for a ticket transaction, and consider what a throbber communicates, 
and/or hides? How might we characterise this icon differently?

It is perhaps a bit paradoxical in design that in a throbber, 
that should not be able to suggest how far you are in the "loadings" process 
and what is actually going on at the back of the waiting time. 
I have tried to solve this by making sure my small circles in the rotation
gradually disappear away and are covered. But still, it gives a sense of length and progress. 
It has both its advantages and disadvantages, as you feel both entertained, enlightened about process, *
but completely the opposite has not really information has received in fact.




https://magnusbak.gitlab.io/ap2020/MiniX3/


![Screenshot](Skyline.png)