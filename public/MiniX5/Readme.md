https://magnusbak.gitlab.io/ap2020/MiniX5

Revisit one of the earlier mini exercises that you have done, and try to rework on your design, both technically and conceptually.

* what is the concept of your work?
* what is the departure point?
* what do you want to express?
* what have you changed and why?

Reflect upon what Aesthetic Programming might be (Based on the readings that you have done before 
(especially this week on Aesthetic Programming as well as the class01 on Why Program? and Coding Literacy),
What does it mean by programming as a practice, or even as a method for design? 
Can you draw and link some of the concepts in the text and expand with your critical take? What is the relation between programming and digital culture?

In this week’s miniX assignment it was a huge liberation of being exempt from a concrete task definition, so it was a free framework, to put together one self’s work. 
There was little opportunity to create your own small design without actually having a real vision or idea from the beginning.

I've chosen a little to include components from all the previous miniX's, to try on all the tools, gears and features we've been doodling with in the past few weeks.
My design is based on the first MiniX1, where you had to try with shapes, colors, where I personally thought that my own back then was a bit poor. 
Therefore, I have redesigned the structure, shapes, colors and applied them with interactivity and movement. 
It mainly serves as my own rebirth, where I can properly test the features; loops, if statements, 
rotation (Both x, y and z axis), ambientlight, directional light and new shapes; Sphere, Torus. 
In addition, I have implanted the camera, not because as such it should actually have any function, but most of all, 
for it prove to myself that I have not hostilely-done it for myself. The idea is to become familiar with all the functions, so they are at hand, whenever I need them.

The design itself; It is most reminiscent of a trip to the outer corners of the galaxy, where all life, light and joy has actually disappeared, 
but here you find yourself, where there is room for a little abstract thoughts and various small angular inputs. 
It's my mini spectacle of a small constellation where planets, stars, wormholes hover around, while these are shapes and figures, we can't imagine yet, living among them.

The design takes up setting and shape in outer space, where the interactive aspect is structured by the outlines of the colors and the transparency. As the program is set to choose randomly between conditions, a new one appears every single time. It will be a bit abstract and artistic, but personally I think it has succeeded very well, creating a "universe" with slightly different fills and where one is challenged on the fantasies. It should simulate how a "wormhole" eats and swallows everything around, where all material, life and light eventually disappears until nothing more is found.
