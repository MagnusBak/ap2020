var x=0
var x2=5
var speed =20
var speed2=20
let change = false

function setup(){
createCanvas(1200, 800, WEBGL);
  let c = createCanvas(800, 600);
  c.position(0,0);
  let capture = createCapture();
  capture.size(80,80);
  capture.position(100,100);
}
function draw() {

background(0);
 trek()

 if(x<0|| x>1000){
   speed=speed*-1;
   }
   x=x+speed;
   push();
   translate(400,-200, -200);
   sphere(300);
   pop();

   push();
   translate(-400,200, -200);
   sphere(300);
   pop();
// Fly function for Stars on X-akse
 push();
  translate(0, 0);
  rotate(frameCount / 100.0);
  rotateX(millis()/1000)
  fly(150, 1, 5, 50, 5);
  pop();
// - X
  push();
   translate(0, 0);
   rotate(frameCount / 350.0);
   rotateX(millis()/1500)
   fly(150, 1, 5, 50, 5);
   pop();
// -X
   push();
    translate(0, 0);
    rotate(frameCount / 300.0);
    rotateX(millis()/2000)
    fly(150, 1, 5, 50, 5);
    pop();
//-----
    push();
     translate(0, 0);
     rotate(frameCount / 250.0);
     rotateX(millis()/2500)
     fly(150, 1, 5, 50, 5);
     pop();
//-----
     push();
      translate(0, 0);
      rotate(frameCount / 200.0);
      rotateX(millis()/3000)
      fly(150, 1, 5, 50, 5);
      pop();
//---
  push();
   translate(0,0);
   rotate(frameCount / 150.0);
   rotateX(millis()/1500)
   fly(-150, 1, 5, 50, 5);
   pop();
//----
   push();
    translate(0,0);
    rotate(frameCount / 200.0);
    rotateX(millis()/2000)
    fly(-150, 1, 5, 50, 5);
    pop();
//------
    push();
     translate(0,0);
     rotate(frameCount / 300.0);
     rotateX(millis()/2500)
     fly(-150, 1, 5, 50, 5);
     pop();
//-----
     push();
      translate(0,0);
      rotate(frameCount / 250.0);
      rotateX(millis()/3000)
      fly(-150, 1, 5, 50, 5);
      pop();

// ------
      push();
       translate(0,0);
       rotate(frameCount / 350.0);
       rotateX(millis()/3000)
       fly(-150, 1, 5, 50, 5);
       pop();

       push();
        translate(0,50);
        rotate(frameCount / 300.0);
        rotateX(millis()/3500)
        fly(-150, 1, 5, 50, 5);
        pop();

        push();
         translate(0,50);
         rotate(frameCount / 400.0);
         rotateX(millis()/4000)
         fly(-150, 1, 5, 50, 5);
         pop();

         push();
          translate(0,50);
          rotate(frameCount / 350.0);
          rotateX(millis()/3500)
          fly(150, 1, 5, 50, 5);
          pop();

          push();
           translate(0,50);
           rotate(frameCount / 400.0);
           rotateX(millis()/4000)
           fly(150, 1, 5, 50, 5);
           pop();

   push();
 translate(-7,0);
 rotate(frameCount / 200.0);
 fly(0, 0, 5, 70, 150);
 pop();

 push();
translate(250,200);
rotate(frameCount / -100.0);
fly(0, 0, 30, 70,6);
pop();


push();
translate(250,200);
rotate(frameCount / -100.0);
fly(0, 0, 20, 50,6);
pop();

push();
translate(250,200);
rotate(frameCount / -100.0);
fly(0, 0, 10, 25,6);
pop();

//new star
push();
rotate(PI/-2)
translate(250,-200);
rotate(frameCount / -100.0);
fly(0, 0, 20, 50,6);
pop();
// New star
push();
rotate(PI/-2)
translate(250,-200);
rotate(frameCount / -100.0);
fly(0, 0, 30, 70,6);
pop();

//----
push();
rotate(PI/-2)
translate(250,-200);
rotate(frameCount / -100.0);
fly(0, 0, 20, 50,6);
pop();
//----
push();
rotate(PI/-2)
translate(250,-200);
rotate(frameCount / -100.0);
fly(0, 0, 10, 25,6);
pop();
//----
push();
 translate(380,-200, -200);
 rotateZ(frameCount * 0.05);
 rotateX(frameCount * 0.05);
 rotateY(frameCount * 0.05);
 torus(80, 20, 64, 64);
 pop();

 push();
  translate(380,-200, -200);
  rotateZ(frameCount * 0.07);
  rotateX(frameCount * 0.07);
  rotateY(frameCount * 0.07);
  torus(80, 20, 64, 64);
  pop();

  push();
   translate(380,-200, -200);
   rotateZ(frameCount * 0.1);
   rotateX(frameCount * 0.1);
   rotateY(frameCount * 0.1);
   torus(80, 20, 64, 64);
   pop();

//small wormhole 1
push();
translate(280,-40-100);
rotateZ(frameCount * 0.05);
rotateX(frameCount * 0.05);
rotateY(frameCount * 0.05);
torus(40, 10, 40, 40);
pop();

//small wormhole 2
push();
translate(280,-40-100);
rotateZ(frameCount * 0.1);
rotateX(frameCount * 0.1);
rotateY(frameCount * 0.1);
torus(40, 10, 40, 40);
pop()

//Small wormhole 3
push();
translate(280,-40-100);
rotateZ(frameCount * 0.07);
rotateX(frameCount * 0.07);
rotateY(frameCount * 0.07);
torus(40, 10, 40, 40);
pop()
// Torus left corner

push();
 translate(-380,200, -200);
 rotateZ(frameCount * 0.05);
 rotateX(frameCount * 0.05);
 rotateY(frameCount * 0.05);
 torus(80, 20, 64, 64);
 pop();

 push();
  translate(-380,200, -200);
  rotateZ(frameCount * 0.07);
  rotateX(frameCount * 0.07);
  rotateY(frameCount * 0.07);
  torus(80, 20, 64, 64);
  pop();

  push();
   translate(-380,200, -200);
   rotateZ(frameCount * 0.1);
   rotateX(frameCount * 0.1);
   rotateY(frameCount * 0.1);
   torus(80, 20, 64, 64);
   pop();

//small wormhole 1
push();
translate(-280,240-100);
rotateZ(frameCount * 0.05);
rotateX(frameCount * 0.05);
rotateY(frameCount * 0.05);
torus(40, 10, 40, 40);
pop();

//small wormhole 2
push();
translate(-280,240-100);
rotateZ(frameCount * 0.1);
rotateX(frameCount * 0.1);
rotateY(frameCount * 0.1);
torus(40, 10, 40, 40);
pop()

//Small wormhole 3
push();
translate(-280,240-100);
rotateZ(frameCount * 0.07);
rotateX(frameCount * 0.07);
rotateY(frameCount * 0.07);
torus(40, 10, 40, 40);
pop()

//Star planets
push();
translate(250,200);
rotate(frameCount / 350.0);
rotateX(millis()/3000)
fly(0, 0, 100, 15, 50);
pop();

push();
translate(250,200);
rotate(frameCount / 100.0);
rotateX(millis()/3000)
fly(0, 0, 100, 15, 50);
pop();

push();
translate(250,200);
rotate(frameCount / 225.0);
rotateX(millis()/3000)
fly(0, 0, 100, 15, 50);
pop();

}
function fly(x, y, radius1, radius2, npoints) {
  let angle = TWO_PI / npoints;
  let halfAngle = angle / 2.0;
  beginShape();
  for (let a = 0; a < TWO_PI; a += angle) {
    let sx = x + cos(a) * radius2;
    let sy = y + sin(a) * radius2;
    vertex(sx, sy);
    sx = x + cos(a + halfAngle) * radius1;
    sy = y + sin(a + halfAngle) * radius1;
    vertex(sx, sy);
  }
  endShape(CLOSE);
}

function trek(){
  if(x<0|| x>2000){
    speed=speed*-1;
    }
    x=x+speed;

  let locX = mouseX - height / 2;
  let locY = mouseY - width / 2;


  directionalLight(200, 10, 10, 10, 10, 0);
  pointLight(0, 0, 255, locX, locY, 250);

  push();
  ambientLight(200);
  translate(-width / 60, 0, 0   );
  rotateZ(frameCount * 0.03);
  rotateX(frameCount * 0.03);
  rotateY(frameCount * 0.03);
  let step = frameCount % 1000;
  applyMatrix(200 / step, 0, 0, 200 /step, 0, 0,10);
  specularMaterial(2);
  box(x, 60, 25);
  rotate(PI/2)
  box(x, 60, 25);
  pop();


  push();
  ambientLight(200);
  translate(-width / 60, 0, 0   );
  rotateZ(frameCount * 0.07);
  rotateX(frameCount * 0.07);
  rotateY(frameCount * 0.07);
  //rotateY(frameCount * 0.03);
  specularMaterial(2);
  box(x, 50, 25);
  rotate(PI/2)
  box(x, 50, 25);
  pop();

push();
  translate(width / -100, 0, 0);
  rotateZ(frameCount * 0.03);
  rotateX(frameCount * 0.03);
  ambientMaterial(200);
  sphere(70,2);
  rotate(PI/2)
  sphere(70,2);
pop();

//Small Stars
push();
 translate(20,-250);
 rotate(frameCount / 100.0);
 fly(0, 0, 10, 15, 50);
 pop();
 push();
 translate(-260,-158);
 rotate(frameCount / 100.0);
 fly(0, 0, 80, 40, 180);
 pop();
 push();
 translate(0, 250);
 rotate(frameCount / 100.0);
 fly(0, 0, 10, 15, 50);
 pop();

}
function mousePressed() {
  change = !change
  if(change == false){
    noFill()
    stroke(255)
  }
  if (change == true){
  fill(random(200),10,10,random(100))
  }
function touchMoved() {
  line(mouseX, mouseY, pmouseX, pmouseY);
  return false;
}

}
