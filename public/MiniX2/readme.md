Describe your program and what you have used and learnt.

My task response consists of two separate emojis, each of which tries to maintain its own view and its own touch. 
I've been trying to create a completeness through some incomplete sketches and templates. 
The first – left – is an attempt to shape and create something different where you don't always have to have a concrete answer or reaction to something. 
So, following all the si-fi and spacey art, it's rooted in the amination universe – Rick and morty, Final Fantasy, Futurama, Adventure Time, 
that lets the observer live in the same abstract and "creative" mindset.
The right - is a bit of a different genre, as it must illustrate a merger between; 
the imaginative, mythological, human and contemporaries something that is very recognizable to us, and at the same time is very far away. 
It must act as an eye opener and questions ask as it exudes a reaction of "surprise", "exclamation" and "attention". 
The idea, symbolism and combination of elements stem from a world that characterizes a galaxy far far away.

How would you put your emoji into a wider cultural context that concerns representations, identity, race, social, economics, culture, device politics and beyond? 
(Try to think through the assigned reading and your coding process, and then expand that to your experience and thoughts - 
this is a difficult task, you may need to spend sometimes in thinking about it).


Emojis have become a central part of the universal language, both electronic, symbolism and art. 
Relationships and associations have been created for different characters and images. 
For example, when you draw hearts, stars or just some lines, the recipient understands what the sender means by the message. 
But we have been caught in small round circles and various "superficial" symbols. 
If you want to express something controversial, you typically have to combine a variety of emojis to create a greater meaning. 
Especially if you're going to try to create a whole story, you'll most often need 8-10 emojis, 
but it's hard to be able to nuance new and exciting expressions. - Which is a pity.
I think it's important to focus on what role the formability, shades, shapes, edges where marked change happens, 
what just the slightest color difference, minimal change in position or size, 
then a different kind of understanding/perception of the expression of the emoji is created.

RunMe: https://magnusbak.gitlab.io/ap2020/MiniX2/


![ScreenShot](Minix2.png)

<img alt="YAP" src="https://i.imgur.com/C4ys5P8.png">