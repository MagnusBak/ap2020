This week's task response was a bit different than usual to try out new features and concepts. However, it has not succeeded in shaping and completing my final design. Unfortunately, it has been a challenge to get the program to follow my thinking. So, this ended up being my sketch, but far from the final product. But I've still had fun creating and going through the process.
The concept of the game: It was intended that the program should have been a form of reverse "snake game", where you had to control the head of a snake via the arrow keys, where you had to collect(capture) all the other snakes that are freely on the screen. Then you had to eat through their old "lines", where you would eventually be able to create a clearance in the middle of the screen, so that the text "Victory" would then return as free and alone, then you have won the level.

1.	Next level. It should be possible to increase the level (Difficulty) by pressing the mouse, then "clear" the screen so that it is possible to start again, where one must repeatedly clean the screen of "snakes".
1.	Twist: Every time you press the mouse, so even if it cleans the track, it adds another snake to the game, at the point where you press the screen.

To processes:'
'
I have managed to get work with some different kinds of "for lops" and with many different variables. In addition, I have used some elements that have entered into our previous tasks, such as: throbber – which are the pink circles that run around the screen to indicate where the finish line is. I have also tried to play a bit with lines, how to vary in degrees, width/length, which has been very entertaining.
Then I have defined different classes, which have functioned as independent and self-determining functions; One class has a blueprint for describing a "snakes" – which is called "balls" and the other function is "morty" which is the very appearance of the snakes, so they look like little devils.
Games: 
The program fulfills many of the values that characterize a game and its conceptual understanding. It's a game where you yourself kind of help define how it should start, finish and process along the way. For most of all, it's like a kind of drawing game where it's all about filling the whole canvas. But as long as it is interactive and the user himself has a meaningful role in creating an overall context, in my eyes it is always a "game".

Personally, I think the task has been super interesting this week, but too bad that the corona virus had to ruin our consultation with the instructors, because this task was the first time I've felt that a little help was needed. But otherwise super entertaining to elaborate.

https://magnusbak.gitlab.io/ap2020/MiniX6