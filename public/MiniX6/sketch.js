var balls = []; // empty array
let change = false
var theta;
var speed;
var circleX = 200;
var circleY = 200;

function setup() {
  createCanvas(1200, 600);

  // for loop is used to create multiple ball objects
	for (var i = 0; i < 10; i++) {
    balls[i] = new Ball(width/2, height/2);
  }
r = height * 0.5;
theta = 0;
speed = .02;
}

function draw() {

  fill(120,20,20)
  ellipse(circleX, circleY, 50, 50);
    if (keyIsPressed) {
     if (keyCode == RIGHT_ARROW) {
      circleX +=5;
     }  else if (keyCode == LEFT_ARROW) {
       circleX -= 5;
     } else if (keyCode == UP_ARROW) {
       circleY -= 5;
     } else if (keyCode == DOWN_ARROW) {
       circleY +=5;
     }
    }

textSize(150);{
fill(255)
textAlign(CENTER);
text('VICTORY', 600, 300);
}

push();
noStroke();
fill(255)
//ellipse(600,100,300,50,20)
pop()
points 			= 20 					//number of points
pointAngle 	= 150/points; //angle between points
radius 			= width/3; 		//length of each line from centre to edge of circle

for (angle=100;angle<360;angle=angle+pointAngle){
  x = cos(radians(angle)) * radius; //convert angle to radians for x and y coordinates
  y = sin(radians(angle)) * radius;
  line(radius, radius, x+radius, y+radius); //draw a line from each point back to the centre
}
  for(let i = 0; i < width; i+=20){
    line(10,i,i,i)
    line(i, 10, i, i);
  }
  for (var i = 0; i < balls.length; i++) {
    balls[i].update();
    balls[i].show();
    balls[i].bounce();
  }
  translate(width/2, height/2);
  translate(0,0)
  var x = r * cos(theta);
  var y = r * sin(theta);
  fill(244,132,140 );
  ellipse(x, y, 32, 32);
  theta += speed;
  rotate(PI)
  ellipse(x, y, 32, 32);
}


function mousePressed() {

  balls.push( new Ball(mouseX, mouseY) );

  change = !change
  if(change == false){
background(255)
  }
  if (change == true){
background(0)
}
}

// constructor function is the same.
// it's a blueprint for creating many balls.
function Ball(x, y) {
	this.x = x;
	this.y = y;
	this.sz = 30 ;
	this.xspeed = random(-5, 2);
	this.yspeed = random(-2, 5);

	this.update = function() {
		this.x += this.xspeed;
		this.y += this.yspeed;
	};

	this.show = function() {
		fill(100);
		stroke(20);
		morty (this.x, this.y, this.sz, this.sz);
	};

	this.bounce = function() {
		if (this.x > width || this.x < 0) {
			this.xspeed *= -1;
		}
		if (this.y > height || this.y < 0) {
			this.yspeed *= -1;
		}
	}
  function morty (x, y, diam) {
      push();
      fill(30, 200, 0);
      stroke(0);
      strokeWeight(2);
      ellipse(x,y,1,40)
      ellipse(x, y, diam, diam);
      ellipse(x,y,10,10)
      pop()

      var start = .2*PI
      var end = .8*PI
      var sDiam = .3*diam;
      arc(x, y, sDiam, sDiam, start, end);


      var offset = 0.3*diam;
      var offset2 = 0.4*diam;
      var eye = 0.1*diam;
      rect(x-offset, y-offset, eye, eye);
      rect(x+offset, y-offset, eye, eye);
      rect(x-offset2, y-offset2, eye, eye);
      rect(x+offset2, y-offset2, eye, eye);
}
}
