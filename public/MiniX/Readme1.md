• How would you describe your first independent coding experience (in relation to thinking, reading, copying, modifying, writing code, and so on)?
•
My first experience of coding provided a significant insight into the complex - however structured form as codes and the system is set in. Coding must be learned with trials and chances it cannot be read to.
It gave me a huge amount of help just to jump into it and otherwise just google and even find my mistakes. It is also a great help to have p5.js reference list as a support.
At first, my focus was to learn the basic functions; Shapes, Colors, Brightness and Generally to understand sizes and positioning on canvas. In addition, I have tried my hand at a little movement and rotation.

• How is the coding process different from, or similar to, reading and writing text?
All kinds of languages have a rhythm, structure and the pattern of transparency as soon as you get the feeling and start to see through the system, it makes a bit of sense.
It is very similar to when you had to learn English and French in primary school, just on their own now.
 A significant difference is if you have written something wrong in your programming structure or making a spelling mistake, then you may have to start over.

• What does code and programming mean to you, and how do the assigned readings help you to reflect on programming?
It provides an overall overview of programming, but I get the most out of the videos on youtube with David.
But it is, of course, only the first week, counting on the texts to be a huge help in the future.
RunMe https://magnusbak.gitlab.io/ap2020/MiniX/


ScreenShot:

![Screenshot](Minixpic1.png)

<img alt="YAP" src="https://i.imgur.com/sokPVek.png">

RunMe: https://magnusbak.gitlab.io/ap2020/MiniX/

Sketch: https://gitlab.com/MagnusBak/ap2020/-/blob/master/public/MiniX/sketch.js
