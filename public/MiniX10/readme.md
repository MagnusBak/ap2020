In this week's minix10, the focus is on the formation of flowcharts and brainstorming of the final exam project. 
 
Avatar-Game-PDF and IPhone-game-PDF are both two drafts of flowcharts around our final project, as a kind of sketch to structure our vision and plans with the ideas.


The Avatar game: is based on the idea that everything in the late modern society is built around the subjective self-image and identity formation that puts the individual at the center. There is the extensive debate and discussion around career, sexuality, religion, upbringing, appearance. Etc, which helps to shape the persona of the individual. Therefore, we have looked at the provocation of self-realization, if one begins to give the different identity elements value criteria and then puts together your avatar with different factors, then the different future's unfolding.

The game should be seen as a "stats" card (reminiscent of yu-gi-oh map) where you can see your character's various strengths and weaknesses at the bottom. – If you then choose different elements by character, the criteria will vary. Ex. One White Male(20points), Heterosexual(15points) Career Lawyer(20 points), Hawaii attire(-30 points) and Hairless/bald(-20points).. 

So he will end up with a total strength of +5 points. On a scale of 1-100.

The game should be seen in relation to the topic of emojis and various forms of self-understanding on the web, as well as identity formations through avatar/online profiles (facebook, twitter) and the many prejudices that come with it.

The iPhone-Message game: is based on the idea of creating some kind of story narrative through an iphone messenger, where there must be the possibility of different outcomes depending on what kind of response one chooses in response in a multiple choice. The focus must be on the experience value and empathy, through interactivity, as well as the feeling and reaction of a story that takes place on iphone, where the sender is a foreign person who tries to hack into the network of use because one has accepted an "incorrect" third party to access one's personal information. – Thus, the themes are; bigdata and data capture, as well as interactivity(experience) and Games (Class).

•  What are the challenges for the two ideas and how are you going to address them? 
1.	The Avatar game can become incredibly advanced and very abstract, if there is no clear framework set, likewise it can become a bit challenging with the structure of the program. As far as Iphone storytelling game is concerned, the idea, as well as the socially critical and cultural aspect, is open to debate, as there is broad knowledge of the issues surrounding data capture. On the other hand, the program itself may end up consisting of for loops and having classes defined (however, there is scope for innovative ideas in this area)


•  What is the value of the individual and the group flowchart that you have produced?
1.	There is a marked difference in perception and individual understanding of the idea of vision and how the whole of the project should be considered. It is both an excellent communication tool for the individual and straightforward issue, whereas the complex and more advanced issue can be characterized by subjectivity, shortcomings and indirect perceptions. – Flowchart works perfectly for step by step manuals.


The last flowchart ismy answer to minix5, where one of the previous task responses was to be redesigned. This is the one where I have included the most functions, components and various elements, where it becomes most technical and still challenging. I'm not sure my answer to the flowchart is correct, but at least I've had my powers tested with flowcharts. 
