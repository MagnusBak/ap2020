function setup() {
  createCanvas(600, 400);
  noCursor();
}

function draw() {
  background(102);
  ellipse(mouseX, mouseY, 60, 60);

  rectMode(CENTER);
  strokeWeight(19);
  stroke(0, 10, 55);
  fill(255, 204, 100);
  rect(300, 200, 350, 150);

  noFill();
stroke(255, 102, 0);
bezier(475, 125, 260,20, 350, 10, 125, 120);

  line(140,140,460,260)
  line(140,260,460,140)
  noStroke();
  fill(300, 150,800, 150);
  ellipse(300, 200, 300, 300);
  fill(300, 280, 600, 175);
  ellipse(150, 200, 300, 300);
  fill(150, 280, 100, 175);
  ellipse(450, 200, 300, 300);

  push();
translate(width * 0.2, height * 0.5);
rotate(frameCount / 100.0);
star(50, 50, 10, 170, 25);
pop();
push();
 translate(width * 0.8, height * 0.5);
 rotate(frameCount / -100.0);
 star(50, 50, 10, 170, 25);
 pop()
}
function star(x, y, radius1, radius2, npoints) {
  let angle = TWO_PI / npoints;
  let halfAngle = angle / 2.0;
  beginShape();
  for (let a = 0; a < TWO_PI; a += angle) {
    let sx = x + cos(a) * radius2;
    let sy = y + sin(a) * radius2;
    vertex(sx, sy);
    sx = x + cos(a + halfAngle) * radius1;
    sy = y + sin(a + halfAngle) * radius1;
    vertex(sx, sy);
  }
  endShape(CLOSE);
}
function star(x, y, radius1, radius2, npoints) {
  let angle = TWO_PI / npoints;
  let halfAngle = angle / 2.0;
  beginShape();
  for (let a = 0; a < TWO_PI; a += angle) {
    let sx = x + cos(a) * radius2;
    let sy = y + sin(a) * radius2;
    vertex(sx, sy);
    sx = x + cos(a + halfAngle) * radius1;
    sy = y + sin(a + halfAngle) * radius1;
    vertex(sx, sy);
  }
  endShape(CLOSE);
}
